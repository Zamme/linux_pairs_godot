extends Node


const TOTAL_MATCH_TIME = 200

var currentTime : float
var initTime : float

var cardsAvailable : Array
var reverseImage : Sprite
var cardButtonPrefab : Node

var cardsButtons : Array

var cardsIndexs : Array

var lastCard : Node

var successes : int
var leftTime : int

enum GameState { InGame, Won, Lost, Waiting, Started }
var gameState = GameState.Waiting

# UI links
var mainCanvas : Node
var mainPanel : Node
var cardsPanel : Node
var scoresPanel : Node
var newHighScorePanel : Node
var areYouSurePanel : Node
var lostPanel : Node

enum PanelType { Main, Cards, Scores, NewHighScore, AreYouSure, Lost }

# UI Effects
var highScoresNameLabels : Array
var highScoresScoreLabels : Array
var highScoresDoneButton : Array
var highScoresResetButton : Array

#public ULP_HighScores highScores;

var soundManager : Node

enum Language { Cat, Esp, Eng }
var language = Language.Eng
#private LocalizationManager localizationManager;
#private GameObject localizationManagerObj;

#public string[] languagesFilenames;

#private ULP_HighScores.HighScore highScorePendingToAdd;

var firstGame : bool = true

var cards_grid_container : GridContainer
var pairs_count : int
var available_pairs_count : int
var columns_count : int
var pairs_count_slider : Slider

var are_you_sure : ConfirmationDialog

var current_cards : Array

var button_size : Vector2
var min_button_size : Vector2

const CARDS_DIRPATH = "res://src/scenes/cards/"
const REVERSE_FILEPATH = "res://src/scenes/reverses/card_linux.tscn"
const TIME_CARDS_VIEW = 3.0
const TIME_WRONGPAIR_HIDE = 2.0
const TIME_MATCH = 50.0

var reverse_icon = preload("res://assets/images/reverses/card_linux.png")

var hide_cards_timer : Timer
var wrong_pair_timer : Timer
var match_timer : Timer

var cards_selected : Array

var pairs_aquired : int

var match_time_label : Label

var end_match_popup : AcceptDialog = preload("res://src/scenes/EndMatchPopup.tscn").instance()


func _ready():
	#GetLocalizationManager()
	#SetLanguage(2) # English is the default language
	#SetSoundManager()
	#LoadHighScores(false)
	#GetCanvass()
	#GetGameStatsLabels()
	#GetHighScoresPanelButtons()
	get_available_cards()
	update_pairs_slider()
	cards_grid_container = get_node("VBoxContainer/HBoxContainer/CardsGridContainer")
	setup_hidecardstimer()
	setup_wrongpairtimer()
	setup_matchtimer()
	setup_matchtimelabel()
	add_child(end_match_popup)
	#StartGame()


func add_card_selected(card : Button):
	cards_selected.append(card)
	if len(cards_selected) == 2:
		check_cards_selected()


func calc_button_size():
	print(cards_grid_container.rect_size)
	var columns : int = cards_grid_container.columns
	if columns < 6:
		columns = 6
	var square_size = cards_grid_container.rect_size.x / (columns+1)
	button_size = Vector2(square_size, square_size)


func clean_grid_container():
	for child in cards_grid_container.get_children():
		cards_grid_container.remove_child(child)
		child.queue_free()


func check_cards_selected():
	if cards_selected[0].hide_icon == cards_selected[1].hide_icon:
		print("parella feta")
		pairs_aquired += 1
		reset_cards_selected()
		if pairs_aquired == pairs_count:
			end_match(true)
	else:
		print("cagada")
		wrong_pair_timer.start()


func dir_contents(path):
	var dir_filepaths : Array
	var dir = Directory.new()
	if dir.open(path) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if dir.current_is_dir():
				print("Found directory: " + file_name)
			else:
				print("Found file: " + file_name)
				dir_filepaths.append(file_name)
			file_name = dir.get_next()
	else:
		print("An error occurred when trying to access the path.")
	
	return dir_filepaths


func disable_all_cards():
	for card in current_cards:
		card.disabled = true


func enable_all_cards():
	for card in current_cards:
		card.disabled = false


func end_match(win : bool):
	if win:
		print("Has guanyat")
		end_match_popup.dialog_text = "YOU WIN!"
	else:
		print("Has perdut")
		end_match_popup.dialog_text = "YOU LOSE!"
	disable_all_cards()
	match_timer.stop()
	end_match_popup.show()


func get_available_cards():
	var dir_filepaths = dir_contents(CARDS_DIRPATH)
	for fp in dir_filepaths:
		cardsAvailable.append(load(CARDS_DIRPATH + fp))
	available_pairs_count = len(dir_filepaths)
	pairs_count = available_pairs_count


func load_cards():
	clean_grid_container()
	cards_grid_container.set_columns(pairs_count)
	calc_button_size()
	current_cards.clear()
	for i_card in pairs_count:
		for ind in 2:
			var new_card : Button = cardsAvailable[i_card].instance()
			new_card.rect_min_size = button_size
			current_cards.append(new_card)
	randomize()
	current_cards.shuffle()
	for ca in current_cards:
		cards_grid_container.add_child(ca)


func quit_app():
	get_tree().quit()


func quit_question():
	if are_you_sure == null:
		are_you_sure = load("res://src/scenes/AreYouSureToQuit.tscn").instance()
		add_child(are_you_sure)
	are_you_sure.popup()
	are_you_sure.visible = true


func reset_cards_selected():
	cards_selected.clear()


func reset_pairs_aquired():
	pairs_aquired = 0


func setup_hidecardstimer():
	hide_cards_timer = get_node("HideCardsTimer")
	hide_cards_timer.wait_time = TIME_CARDS_VIEW
	hide_cards_timer.one_shot = true


func setup_matchtimelabel():
	match_time_label = get_node("VBoxContainer/HBoxContainer2/VBoxContainer/MatchTimeLabel")
	

func setup_matchtimer():
	match_timer = get_node("MatchTimer")
	match_timer.wait_time = TIME_MATCH
	match_timer.one_shot = true


func setup_wrongpairtimer():
	wrong_pair_timer = get_node("WrongPairTimer")
	wrong_pair_timer.wait_time = TIME_WRONGPAIR_HIDE
	wrong_pair_timer.one_shot = true


func stop_all_timers():
	hide_cards_timer.stop()
	wrong_pair_timer.stop()
	match_timer.stop()


func update_matchtimelabel():
	match_time_label.text = str(int(match_timer.time_left))


func update_pairs_count():
	pass


func update_pairs_slider():
	pairs_count_slider = find_node("PairsCount")
	pairs_count_slider.value = available_pairs_count
	pairs_count_slider.max_value = available_pairs_count


func _process(delta):
	update_matchtimelabel()


func StartGame():
	#CleanCards()
	#FillCardsIndexs()
	#SortIndexes()
	#CreateCards()
	#ResetStats()
	#UpdateStats()
	#soundManager.PlayAudio(ULP_SoundManager.AudioSample.GameMusic)
	StartMatch()


func StartMatch():
	stop_all_timers()
	load_cards()
	hide_cards_timer.start(TIME_CARDS_VIEW)
	reset_cards_selected()

"""
	ShowMainPanel()
	
	if (firstGame):
		SetGameState(GameState.Waiting)
		firstGame = false
	else:
		SetGameState(GameState.Started)


func ActivateCardsButtons(activate : bool):
	var buttons = cardsPanel.get_children();

	for but in buttons:
		but.interactable = activate;


func CardPressed(card : Node):
	soundManager.PlayAudio(ULP_SoundManager.AudioSample.CardClick);
	soundManager.PlayAudio(ULP_SoundManager.AudioSample.CardReveal);

	var cardButton : Node = card.GetComponent<Button>();
	ULP_Card cardBehaviour = card.GetComponent<ULP_Card>();
	int cardIndex = int.Parse(card.name);

	if (lastCard)
	{
		Button lastCardButton = lastCard.GetComponent<Button>();
		ULP_Card lastCardBehaviour = lastCard.GetComponent<ULP_Card>();
		int lastCardIndex = int.Parse(lastCard.name);

		if (cardIndex != lastCardIndex)
		{
			GameObject[] cards = new GameObject[2];
			cards[0] = card;
			cards[1] = lastCard;
			if (cardBehaviour.cardValue == lastCardBehaviour.cardValue)
			{
				successes++;
				lastCardButton.interactable = false;
				cardButton.interactable = false;
				StartCoroutine(Succes(cards));
			}
			else
			{
				StartCoroutine(Fail(cards));
			}
		}
		lastCard = null;
	}
	else
	{
		lastCard = card;
	}


void CheckGameState()
{
	if (successes == cardsImages.Length)
	{
		gameState = GameState.Won;
	}

	if (leftTime < 1)
	{
		SetGameState(GameState.Lost);
	}
}

void CleanCards()
{
	GameObject[] cardsCreated = GameObject.FindGameObjectsWithTag("Card");
	foreach (GameObject card in cardsCreated)
	{
		Destroy(card);
	}
}

void CreateCards()
{
	cardsPanel = GetCardsPanel();
	if (cardsPanel)
	{
		cardsButtons = new Button[cardsImages.Length * 2];
		for (int buttonIndex = 0; buttonIndex < cardsButtons.Length; buttonIndex++)
		{
			GameObject cardBut = Instantiate(cardButtonPrefab) as GameObject;
			cardsButtons[buttonIndex] = cardBut.GetComponent<Button>();
			cardBut.transform.parent = cardsPanel.transform;
			cardBut.name = buttonIndex.ToString();
			cardBut.GetComponent<ULP_Card>().SetCardValue(cardsIndexs[buttonIndex]);
			cardBut.tag = "Card";
			cardBut.layer = 9;
		}
	}
}

private void EnableButton(GameObject buttonObject, bool enable)
{
	Button button = buttonObject.GetComponent<Button>();
	if (button)
	{
		button.interactable = enable;
	}
	else
	{
		Debug.Log("No button component on " + buttonObject.name);
	}
}

public void EnablePanel(PanelType panelType, bool enable)
{
	switch (panelType)
	{
		case PanelType.Main:
			mainPanel.SetActive(enable);
			break;
		case PanelType.Cards:
			cardsPanel.SetActive(enable);
			break;
		case PanelType.Scores:
			scoresPanel.SetActive(enable);
			break;
		case PanelType.NewHighScore:
			newHighScorePanel.SetActive(enable);
			break;
		case PanelType.AreYouSure:
			areYouSurePanel.SetActive(enable);
			break;
		case PanelType.Lost:
			lostPanel.SetActive(enable);
			break;
	}

	TranslateAll();
}

public void EntryNewRecord()
{
	EnablePanel(PanelType.Scores, true);
	EnablePanel(PanelType.Main, false);
	ShowNewRecordPanel();
}

IEnumerator Fail(GameObject[] cards)
{
	soundManager.PlayAudio(ULP_SoundManager.AudioSample.Fail00);
	yield return new WaitForSeconds(1);
	foreach (GameObject card in cards)
	{
		card.SendMessage("ResetCard");
	}

	soundManager.PlayAudio(ULP_SoundManager.AudioSample.Fail01);
}

void FillCardsIndexs()
{
	cardsIndexs = new List<int>();
	cardsIndexs.Clear();
	for (int nCards = 0; nCards < (cardsImages.Length * 2); nCards++)
	{
		cardsIndexs.Add(GetIndexNormalizedPosition(nCards));
	}
}

void GetCanvass()
{
	mainCanvas = GameObject.Find("MainCanvas");
	mainPanel = GameObject.Find("MainPanel");
	cardsPanel = GameObject.Find("CardsPanel");
	scoresPanel = GameObject.Find("HighScores");
	newHighScorePanel = GameObject.Find("NewHighScore");
	areYouSurePanel = GameObject.Find("AreYouSure");
	lostPanel = GameObject.Find("YouLoose");

	if (mainCanvas && mainPanel && cardsPanel && scoresPanel && newHighScorePanel && areYouSurePanel && lostPanel)
	{
		//Debug.Log("All canvas found");
	}
	else
	{
		Debug.LogError("Some canvas not found");
	}
}

void GetHighScoresLabels()
{
	highScoresNameLabels = GameObject.FindGameObjectsWithTag("PlayerNameLabel");
	highScoresScoreLabels = GameObject.FindGameObjectsWithTag("PlayerScoreLabel");
}

void GetHighScoresPanelButtons()
{
	highScoresDoneButton = GameObject.Find("HSContinueButton");
	if (highScoresDoneButton)
	{
		Button button = highScoresDoneButton.GetComponent<Button>();
		//button.interactable = !show;
	}

	highScoresResetButton = GameObject.Find("ResetHighScores");
	if (highScoresResetButton)
	{
		Button button = highScoresResetButton.GetComponent<Button>();
		//button.interactable = !show;
	}

}

void GetLocalizationManager()
{
	if (!localizationManager)
	{
		if (!localizationManagerObj)
		{
			localizationManagerObj = new GameObject("LocalizationManager");
		}
		localizationManager = localizationManagerObj.AddComponent<LocalizationManager>();
		//Debug.Log("Localization Managaer created.");
	}
}

GameObject GetCardsPanel()
{
	GameObject pa = GameObject.Find("CardsPanel");
	if (pa)
	{
		//Debug.Log("CardsPanel panel found");
	}
	else
	{
		//Debug.Log("CardsPanel panel not found");
	}

	return pa;
}

int GetIndexNormalizedPosition(int index)
{
	int num = index;

	if (num >= cardsImages.Length)
	{
		num = (num % cardsImages.Length);
	}

	return num;
}

void GetGameStatsLabels()
{
	scoreText = GameObject.Find("Score").GetComponent<Text>();
	pairsLeftText = GameObject.Find("Tries").GetComponent<Text>();
	//state = GameObject.Find("State").GetComponent<Text>();
}

public void LoadHighScores(bool withUI)
{
	highScores = new ULP_HighScores();
	highScores.ReloadHighScores(withUI);
}

private void LoadLanguage(int lang)
{
	localizationManager.LoadLocalizedText(languagesFilenames[lang]);
}

public void Quit()
{
	Application.Quit();
}

/// <summary>
/// Resets all. DEPRECATED
/// </summary>
public void ResetAll()
{
	SceneManager.LoadScene("StartupManager");
}

public void ResetRecords()
{
	highScores.ResetHighScores();
	highScores.LoadScoresToPlayerPrefs();
	highScores.SavePlayerPrefsRecords();
	ShowMainPanel();
}

void ResetTimer()
{
	currentTime = Time.time;
	initTime = Time.time;
}

void ResetStats()
{
	successes = 0;
	leftTime = TOTAL_MATCH_TIME;
	lastCard = null;
}

public void SaveNewRecord()
{
	highScorePendingToAdd.SetPlayerName(GameObject.Find("NewRecordPlayerName").GetComponent<Text>().text);
	highScores.SaveRecord(highScorePendingToAdd);
	EnablePanel(PanelType.NewHighScore, false);
	ShowRecordsPanel();
}

void SetGameState(GameState state)
{
	gameState = state;

	switch (state)
	{
		case GameState.InGame:
			ActivateCardsButtons(true);
			break;
		case GameState.Waiting:
			ActivateCardsButtons(false);
			break;
		case GameState.Started:
			StartCoroutine("StartTimer");
			SetGameState(GameState.InGame);
			break;
		case GameState.Lost:
			ActivateCardsButtons(false);
			ShowYouLost();
			break;
	}
}

/// <summary>
/// Sets the language. 0=cat, 1=esp, 2=eng
/// </summary>
/// <param name="lang">Lang.</param>
public void SetLanguage(int lang = 2)
{
	switch (lang)
	{
		case 0:
			language = Language.Cat;
			break;
		case 1:
			language = Language.Esp;
			break;
		case 2:
			language = Language.Eng;
			break;
	}

	LoadLanguage(lang);
	TranslateAll();
}

private void SetSoundManager()
{
	soundManagerObject = GameObject.FindGameObjectWithTag("SoundManager");
	if (soundManagerObject)
	{
		soundManager = soundManagerObject.GetComponent<ULP_SoundManager>();
	}
	else
	{
		Debug.LogError("Sound manager not found!");
	}
}

public void ShowAreYouSurePanel()
{
	EnablePanel(PanelType.Main, true);
	EnablePanel(PanelType.Scores, false);
	EnablePanel(PanelType.NewHighScore, false);
	EnablePanel(PanelType.AreYouSure, true);
	EnablePanel(PanelType.Lost, false);
}

public void ShowMainPanel()
{
	EnablePanel(PanelType.Main, true);
	EnablePanel(PanelType.Scores, false);
	EnablePanel(PanelType.NewHighScore, false);
	EnablePanel(PanelType.AreYouSure, false);
	EnablePanel(PanelType.Lost, false);
}

public void ShowRecordsPanel()
{
	EnablePanel(PanelType.Main, false);
	EnablePanel(PanelType.Scores, true);
	EnablePanel(PanelType.NewHighScore, false);
	EnablePanel(PanelType.AreYouSure, false);
	EnablePanel(PanelType.Lost, false);

	ViewHighScores();
}

public void ShowNewRecordPanel()
{
	ShowRecordsPanel();
	EnablePanel(PanelType.NewHighScore, true);
	EnableButton(highScoresDoneButton, false);
	EnableButton(highScoresResetButton, false);
}

public void ShowYouLost()
{
	EnablePanel(PanelType.Main, true);
	EnablePanel(PanelType.Scores, false);
	EnablePanel(PanelType.NewHighScore, false);
	EnablePanel(PanelType.AreYouSure, false);
	EnablePanel(PanelType.Lost, true);

}

void Shuffle<T>(List<T> list)
{
	System.Random random = new System.Random();
	int n = list.Count;
	while (n > 1)
	{
		int k = random.Next(n);
		n--;
		T temp = list[k];
		list[k] = list[n];
		list[n] = temp;
	}
}

void SortIndexes()
{
	Shuffle<int>(cardsIndexs);
}

IEnumerator Succes(GameObject[] cards)
{
	soundManager.PlayAudio(ULP_SoundManager.AudioSample.Succes00);
	foreach (GameObject card in cards)
	{
		card.SendMessage("EmitSuccesEffects");
	}
	soundManager.PlayAudio(ULP_SoundManager.AudioSample.Succes01);
	yield return new WaitForSeconds(1);
}

IEnumerator StartTimer()
{
	ResetTimer();

	while (leftTime > 0)
	{
		currentTime = (Time.time - initTime);
		leftTime = (TOTAL_MATCH_TIME - (int)currentTime);
		UpdateStats();
		yield return new WaitForEndOfFrame();
	}

	SetGameState(GameState.Lost);
}

void TranslateAll()
{
	localizationManager.TranslateAll();
}

void UpdateStats()
{
	UpdateSuccesses();
	UpdateTimer();

	CheckGameState();

	switch (gameState)
	{
		case GameState.Won:
			//state.text = "You Won!";
			StopCoroutine("StartTimer");
			if (highScores.HighScoreAcquired(leftTime))
			{
				EntryNewRecord();
			}
			break;
		case GameState.Lost:
			//TODO: Lost?
			break;
		case GameState.InGame:
			//state.text = "Playing";
			break;
	}
}

void UpdateSuccesses()
{
	scoreText.text = leftTime.ToString();
}

void UpdateTimer()
{
	pairsLeftText.text = (cardsImages.Length - successes).ToString();
}

public void ViewHighScores()
{
	LoadHighScores(true);
	GetHighScoresLabels();
	SetAllLabelsAlpha(highScoresNameLabels, 0.0f);
	SetAllLabelsAlpha(highScoresScoreLabels, 0.0f);
	HighScoresLabelEffect();
	StartCoroutine("StartPlayerHighScoresLabelsEffect");
}

#region effects
private void HighScoresLabelEffect()
{
	GameObject highScoresTitleLabel = GameObject.Find("HighScoresTitleLabel");
	if (highScoresTitleLabel)
	{
		Outline outline = highScoresTitleLabel.GetComponent<Outline>();
		if (outline)
		{
			StartCoroutine("HighScoreLabelReoutlining", outline);
		}
		else
		{
			Debug.LogError("Not outline effect found in " + highScoresTitleLabel.name);
		}
	}
	else
	{
		Debug.LogError("High scores title label not found");
	}
}

private IEnumerator HighScoreLabelReoutlining(Outline cOutline)
{
	float animTotalTime = 1.0f;
	float animState = 0.0f;
	float maxOutlined = 200.0f;
	float minOutlined = 2.0f;

	while (animState < 1.0f)
	{
		cOutline.effectDistance = new Vector2(Mathf.Lerp(maxOutlined, minOutlined, animState), cOutline.effectDistance.y);
		animState += Time.deltaTime / animTotalTime;
		yield return new WaitForEndOfFrame();
	}

	cOutline.effectDistance = new Vector2(minOutlined, cOutline.effectDistance.y);

}

private void SetAllLabelsAlpha(GameObject[] labels, float alpha)
{
	foreach (GameObject label in labels)
	{
		SetLabelAlpha(label.GetComponent<Text>(), alpha);
	}
}

private void SetLabelAlpha(Text text, float alpha)
{
	text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
}

private IEnumerator StartPlayerHighScoresLabelsEffect()
{
	EnableButton(highScoresDoneButton, false);
	EnableButton(highScoresResetButton, false);

	for (int labelIndex = 0; labelIndex < highScoresNameLabels.Length; labelIndex++)
	{
		PlayerHighScoreLabelAlphaEffect(highScoresNameLabels[labelIndex]);
		PlayerHighScoreLabelAlphaEffect(highScoresScoreLabels[labelIndex]);
		yield return new WaitForSeconds(0.25f);
	}

	EnableButton(highScoresDoneButton, true);
	EnableButton(highScoresResetButton, true);
}

private void PlayerHighScoreLabelAlphaEffect(GameObject label)
{
	Text text = label.GetComponent<Text>();

	if (text)
	{
		StartCoroutine("LabelAlphaEffect", text);
	}
}

private IEnumerator LabelAlphaEffect(Text text)
{
	float animTotalTime = 1.0f;
	float animState = 0.0f;
	float target = 1.0f;
	float initial = 0.0f;

	while (animState < 1.0f)
	{
		text.color = new Color(text.color.r, text.color.g, text.color.b, Mathf.Lerp(initial, target, animState));
		animState += Time.deltaTime / animTotalTime;
		yield return new WaitForEndOfFrame();
	}

	text.color = new Color(text.color.r, text.color.g, text.color.b, target);
}

#endregion
"""



func _on_HideCardsTimer_timeout():
	for card in cards_grid_container.get_children():
		card.hide_card()
	match_timer.start(TIME_MATCH)


func _on_WrongPairTimer_timeout():
	for card in cards_selected:
		card.hide_card()
	reset_cards_selected()


func _on_MatchTimer_timeout():
	end_match(false)
