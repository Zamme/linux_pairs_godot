extends Button


var game_manager : Control

var hide_icon
var is_hide : bool


func _ready():
	game_manager = get_node("//root/MainMenu")
	hide_icon = icon


func hide_card():
	icon = game_manager.reverse_icon
	is_hide = true


func show_card():
	icon = hide_icon
	is_hide = false


func _on_card_button_up():
	if is_hide && len(game_manager.cards_selected)<2:
		show_card()
		game_manager.add_card_selected(self)
