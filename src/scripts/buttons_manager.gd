extends HBoxContainer


var game_manager : Control

var pairs_count : int
var pairs_count_label : Label


func _ready():
	game_manager = get_parent().get_parent()


func get_columns_count():
	return pairs_count


func _on_PairsCount_value_changed(value):
	pairs_count = value
	find_node("PairsCountLabel").text = str(pairs_count)
	game_manager.pairs_count = value


func _on_QuitButton_button_up():
	game_manager.quit_question()


func _on_NewGameButton_button_up():
	game_manager.StartGame()
