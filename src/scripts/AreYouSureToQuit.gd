extends ConfirmationDialog


var game_manager : Control


func _ready():
	game_manager = get_parent()


func _on_AreYouSureToQuit_confirmed():
	game_manager.quit_app()
